import { Injectable } from '@angular/core';

@Injectable()
export class AuthorsService {

  get Authors() {
    return ["author1", "author2", "author3"];
  };

  constructor() { }

}
